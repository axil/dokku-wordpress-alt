ifndef APP_NAME
	APP_NAME = blog
endif

ifndef WORDPRESS_VERSION
	WORDPRESS_VERSION = 4.9.0
endif

ifndef DOKKU_USER
	DOKKU_USER = dokku
endif

ifdef UNATTENDED_CREATION
	DOKKU_CMD = ssh $(DOKKU_USER)@$(SERVER_NAME)
else
	DOKKU_CMD = dokku
endif

CURL_INSTALLED := $(shell command -v curl 2> /dev/null)
WGET_INSTALLED := $(shell command -v wget 2> /dev/null)

.PHONY: all
all: help ## Print the help message

.PHONY: help
help: ## Print the help message
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-36s\033[0m %s\n", $$1, $$2}'

.PHONY: build
build: ## Output deploy instructions. Make sure to define SERVER_NAME, APP_NAME, DOMAIN_NAME
ifndef APP_NAME
	$(error "Missing APP_NAME environment variable, this should be the name of your blog app")
endif
ifndef SERVER_NAME
	$(error "Missing SERVER_NAME environment variable, this should be something like 'dokku.me'")
endif
ifndef DOMAIN_NAME
	$(error "Missing DOMAIN_NAME environment variable, enter the domain name which WordPress will be reached, like 'wordpress.dokku.me'")
endif
ifndef CURL_INSTALLED
ifndef WGET_INSTALLED
	$(error "Neither curl nor wget are installed, and at least one is necessary for retrieving salts")
endif
endif
ifdef CURL_INSTALLED
	@curl -so /tmp/wp-salts https://api.wordpress.org/secret-key/1.1/salt/
else
ifdef WGET_INSTALLED
	@wget -qO /tmp/wp-salts https://api.wordpress.org/secret-key/1.1/salt/
endif
endif
	@sed -i.bak -e 's/ //g' -e "s/);//g" -e "s/define('/$(DOKKU_CMD) config:set $(APP_NAME) /g" -e "s/SALT',/SALT=/g" -e "s/KEY',[ ]*/KEY=/g" /tmp/wp-salts && rm /tmp/wp-salts.bak

ifndef UNATTENDED_CREATION
	## Run the following commands on the server to setup the app:
	@echo ""
	@echo "dokku apps:create $(APP_NAME)"
	@echo ""
	## Setup plugins, themes and uploads persistent storage
	@echo ""
	@echo "mkdir -p /var/lib/dokku/data/storage/$(APP_NAME)/{plugins,themes,uploads}"
	@echo "chown -R 32767:32767 /var/lib/dokku/data/storage/$(APP_NAME)"
	@echo "dokku storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/plugins:/app/public/content/plugins"
	@echo "dokku storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/themes:/app/public/content/themes"
	@echo "dokku storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/uploads:/app/public/content/uploads"
	@echo ""
	## Setup your MariaDB database and link it to your app
	@echo ""
	@echo "dokku mariadb:create $(APP_NAME)-db"
	@echo "dokku mariadb:link $(APP_NAME)-db $(APP_NAME)"
	@echo ""
	## Set the proper environment variables for keys and salts that were
	## generated using the wordpress salt API: https://api.wordpress.org/secret-key/1.1/salt/
	@echo ""
	@cat /tmp/wp-salts
	@echo ""
	## Set up the domain name WordPress will use as its site URL
	@echo ""
	@echo "dokku config:set $(APP_NAME) DOMAIN_NAME=$(DOMAIN_NAME)"
	@echo ""
	## Add the domain name to Dokku's app vhosts
	@echo ""
	@echo "dokku domains:add $(APP_NAME) $(DOMAIN_NAME)"
	@echo ""
	## On your local machine, add the remote dokku URL and push the app
	@echo ""
	@echo "git remote add dokku "dokku@$(SERVER_NAME):$(APP_NAME)""
	@echo "git push dokku master"
else
	@chmod +x /tmp/wp-salts
	$(DOKKU_CMD) apps:create $(APP_NAME)
	$(DOKKU_CMD) storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/plugins:/app/public/content/plugins
	$(DOKKU_CMD) storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/themes:/app/public/content/themes
	$(DOKKU_CMD) storage:mount $(APP_NAME) /var/lib/dokku/data/storage/$(APP_NAME)/uploads:/app/public/content/uploads
	$(DOKKU_CMD) mariadb:create $(APP_NAME)-db
	$(DOKKU_CMD) mariadb:link $(APP_NAME)-db $(APP_NAME)
	@/tmp/wp-salts
	@echo ""
	# Run the following commands on the server to ensure data is stored properly on disk
	@echo ""
	@echo "mkdir -p /var/lib/dokku/data/storage/$(APP_NAME)/{plugins,themes,uploads}"
	@echo "chown -R 32767:32767 /var/lib/dokku/data/storage/$(APP_NAME)"
	@echo ""
	# Now, on your local machine, change directory to your new wordpress app, and push it up
	@echo ""
	@echo "git push dokku master"
endif

.PHONY: destroy
destroy: ## Destroy an existing wordpress installation and outputs undeploy instructions
ifndef APP_NAME
	$(error "Missing APP_NAME environment variable, this should be the name of your blog app")
endif
ifndef SERVER_NAME
	$(error "Missing SERVER_NAME environment variable, this should be something like 'dokku.me'")
endif
ifndef UNATTENDED_CREATION
	# destroy the mysql database
	@echo ""
	@echo "dokku mariadb:unlink $(APP_NAME)-db $(APP_NAME)"
	@echo "dokku mariadb:destroy $(APP_NAME)-db"
	@echo ""
	# destroy the app
	@echo ""
	@echo "dokku -- --force apps:destroy $(APP_NAME)"
	@echo ""
	# run the following commands on the server to remove storage directories on disk
	@echo ""
	@echo "rm -rf /var/lib/dokku/data/storage/$(APP_NAME)"
	@echo ""
else
	# destroy the MariaDB database
	$(DOKKU_CMD) mariadb:unlink $(APP_NAME)-db $(APP_NAME)
	$(DOKKU_CMD) mariadb:destroy $(APP_NAME)-db
	# destroy the app
	$(DOKKU_CMD) -- --force apps:destroy $(APP_NAME)
	# run the following commands on the server to remove storage directories on disk
	@echo ""
	@echo "rm -rf /var/lib/dokku/data/storage/$(APP_NAME)"
	@echo ""
endif
