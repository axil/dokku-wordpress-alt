<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

function fromenv($key, $default = null) {
  $value = getenv($key);
  if ($value === false) {
    $value = $default;
  }
  return $value;
}

$DSN = parse_url(fromenv('DATABASE_URL', 'mysql://username_here:password_here@localhost:3306/database_name_here'));

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', substr($DSN['path'], 1));

/** MySQL database username */
define('DB_USER', $DSN['user']);

/** MySQL database password */
define('DB_PASSWORD', $DSN['pass']);

/** MySQL hostname */
define('DB_HOST', $DSN['host']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         fromenv('AUTH_KEY', 'put your unique phrase here'));
define('SECURE_AUTH_KEY',  fromenv('SECURE_AUTH_KEY', 'put your unique phrase here'));
define('LOGGED_IN_KEY',    fromenv('LOGGED_IN_KEY', 'put your unique phrase here'));
define('NONCE_KEY',        fromenv('NONCE_KEY', 'put your unique phrase here'));
define('AUTH_SALT',        fromenv('AUTH_SALT', 'put your unique phrase here'));
define('SECURE_AUTH_SALT', fromenv('SECURE_AUTH_SALT', 'put your unique phrase here'));
define('LOGGED_IN_SALT',   fromenv('LOGGED_IN_SALT', 'put your unique phrase here'));
define('NONCE_SALT',       fromenv('NONCE_SALT', 'put your unique phrase here'));

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = fromenv('TABLE_PREFIX', 'wp_');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', (bool)fromenv('WP_DEBUG', false));

if ( fromenv('WP_DEBUG') ) {
  // Enable Debug logging to the /content/debug.log file
  define( 'WP_DEBUG_LOG', true );

  // Disable display of errors and warnings in the HTML pages
  define( 'WP_DEBUG_DISPLAY', false );
  @ini_set( 'display_errors', 0 );
}

/**
 * If we're behind a proxy server and using HTTPS, we need to alert
 * Wordpress of that fact.
 *
 * @link https://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
 */
if ( isset($_SERVER['HTTP_X_FORWARDED_PROTO'] )
    && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' )
{
    $_SERVER['HTTPS'] = 'on';
}

/**
 * Define custom paths
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php#Advanced_Options
 */
$protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . getenv('DOMAIN_NAME') . '/w');
define('WP_HOME', $protocol . getenv('DOMAIN_NAME'));
define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');
define('WP_CONTENT_URL', $protocol . getenv('DOMAIN_NAME') . '/content');
//define('UPLOADS', dirname(__FILE__) . '/content/uploads');

// Disable the Plugin and Theme Editor
// @link https://codex.wordpress.org/Editing_wp-config.php#Disable_the_Plugin_and_Theme_Editor
define( 'DISALLOW_FILE_EDIT', true );

// Require SSL for Admin and Logins
// @link https://codex.wordpress.org/Editing_wp-config.php#Require_SSL_for_Admin_and_Logins
define( 'FORCE_SSL_ADMIN', true );

// Disable all automatic updates:
// @link https://codex.wordpress.org/Editing_wp-config.php#Disable_WordPress_Auto_Updates
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// Disable WordPress Core Updates
// @link https://codex.wordpress.org/Editing_wp-config.php#Disable_WordPress_Core_Updates
define( 'WP_AUTO_UPDATE_CORE', false );

// Cleanup Image Edits
// @link https://codex.wordpress.org/Editing_wp-config.php#Cleanup_Image_Edits
define( 'IMAGE_EDIT_OVERWRITE', true );

// Disable post revisions
// @link https://codex.wordpress.org/Editing_wp-config.php#Disable_Post_Revisions
define('WP_POST_REVISIONS', false);

/* -------------------------------------------- */

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/w');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . '/wp-settings.php');
